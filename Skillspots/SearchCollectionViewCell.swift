//
//  SearchCollectionViewCell.swift
//  Skillspots
//
//  Created by Matthew Barge on 11/14/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit

class SearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTextView: UITextView!

    var spots: Spot!
    
    func configureCell(spots: Spot) {
        self.spots = spots
        
        cellTextView.text = self.spots.spotName
        cellImage.image = UIImage(named: "")
    }
}
