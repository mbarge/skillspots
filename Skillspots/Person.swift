//
//  Person.swift
//  Skillspots
//
//  Created by Matthew Barge on 12/10/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import Foundation

class Person {
    private var _personFirstName: String!
    private var _personLastName: String!
    private var _personHome: String!
    
    var personFirstName: String {
        return _personFirstName
    }
    
    var personLastName: String {
        return _personLastName
    }
    
    var personHome: String {
        return _personHome
    }
    
    init(spotLocation: Int, spotName: String, spotDescription: String) {
        self._personFirstName = personFirstName
        self._personLastName = personLastName
        self._personHome = personHome
    }
}