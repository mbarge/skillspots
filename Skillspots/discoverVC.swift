//
//  locationVCUIVViewController.swift
//  Skillspots
//
//  Created by Matthew Barge on 12/10/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit
import MapKit
import Parse
import ParseUI
import Mixpanel

class discoverVC: UIViewController, UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var inSearchMode = false
    
    let regionRadius: CLLocationDistance = 1000
    
    let locationManager = CLLocationManager()
    
    var spots: PFGeoPoint = PFGeoPoint()
    var annoImage: UIImageView!
    var finalSpotImage: UIImage!
    var spotImageArray: [PFFile] = []
    var myAnnoImage: PFFile!
    var locValue:CLLocationCoordinate2D!
    var currentLocation: PFGeoPoint!
    let screenName = "Discover"
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locValue = locationManager.location?.coordinate
        let center = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        currentLocation = PFGeoPoint(location: center)
        mapView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        designDetails()
        self.queryData()
        Mixpanel.sharedInstance().track("Discover screen view")
        Mixpanel.sharedInstance().track("Screen View", properties: ["Screen" : screenName])
        
    }
    
    func designDetails() {
        searchBar.returnKeyType = UIReturnKeyType.Search
        searchBar.enablesReturnKeyAutomatically = false
    }
    
    func queryData() {
        let query = PFQuery(className: "Spots")
        query.selectKeys(["spotName", "spotDescription", "spotLocation", "spotImage"])
        
//        query.whereKey("spotLocation", nearGeoPoint: currentLocation, withinMiles: 5)
        // Retrieve the most recent ones
        query.orderByDescending("createdAt")
        
        // Only retrieve the last hundred
        query.limit = 20
        
        query.findObjectsInBackgroundWithBlock {
            (spot: [PFObject]?, error: NSError?) -> Void in
            if error != nil {
                print(error)
            } else {
                for spot in spot! {
                    // This does not require a network access.
                    let geoPoint = spot["spotLocation"] as! PFGeoPoint
                    let spotTitle = spot["spotName"] as! String
                    let spotImage = spot["spotImage"] as! PFFile
                    self.myAnnoImage = spot["spotImage"] as! PFFile
//                    annoImage.getDataInBackgroundWithBlock { (data, error) -> Void in
//                        if let downloadedImage = UIImage(data: data!) {
//                            let compressedImage = UIImage(data: downloadedImage.thumbnailQualityJPEGNSData!)
//                            self.finalSpotImage = compressedImage
//                        }
//                    }
//                    let originalImage = spot["spotImage"] as! PFFile
//                    let convertedImage = UIImage(data: originalImage)
//                    let finalImage = UIImage(data: convertedImage!.lowestQualityJPEGNSData!)
//                    self.spotImage = finalImage
                    let center = CLLocation(latitude: geoPoint.latitude, longitude: geoPoint.longitude)
                    self.createAnnotationForLocation(spotTitle, location: center, spotImage: spotImage)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func searchQuery() {
        let query = PFQuery(className: "Spots")
        query.selectKeys(["spotName", "spotDescription", "spotLocation", "spotImage"])
        
        // Retrieve the most recent ones
        query.whereKey("spotDescription", containsString: "Test")
        
        query.orderByDescending("createdAt")
        
        // Only retrieve the last hundred
        query.limit = 20
        
        query.findObjectsInBackgroundWithBlock {
            (spot: [PFObject]?, error: NSError?) -> Void in
            if error != nil {
                print(error)
            } else {
                for spot in spot! {
                    // This does not require a network access.
                    let spotTitle = spot["spotName"] as! String
                    let spotImage = spot["spotImage"] as! PFFile
                    self.myAnnoImage = spot["spotImage"] as! PFFile
                    self.tableView.reloadData()
                }
            }
        }

    }
    
//    func configureDetailView(annotationView: MKAnnotationView) {
//        let width = 300
//        let height = 200
//        
//        let snapshotView = UIView()
//        let views = ["snapshotView": snapshotView]
//        snapshotView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[snapshotView(300)]", options: [], metrics: nil, views: views))
//        snapshotView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[snapshotView(200)]", options: [], metrics: nil, views: views))
//        
//        let options = MKMapSnapshotOptions()
//        options.size = CGSize(width: width, height: height)
//        options.mapType = .SatelliteFlyover
//        options.camera = MKMapCamera(lookingAtCenterCoordinate: annotationView.annotation!.coordinate, fromDistance: 250, pitch: 65, heading: 0)
//        
//        let snapshotter = MKMapSnapshotter(options: options)
//        snapshotter.startWithCompletionHandler { snapshot, error in
//            if snapshot != nil {
//                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
//                imageView.image = snapshot!.image
//                snapshotView.addSubview(imageView)
//            }
//        }
//        
//        annotationView.detailCalloutAccessoryView = snapshotView
//    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        locationAuthStatus()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("locationCell", forIndexPath: indexPath) as! locationCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.spotTitle.text = "Cool spot"
        cell.spotDescription.text = "Spot description included here in the immediate area of this reusable cell created by Swift code and Apple's iOS operating system."
        cell.spotImage.image = UIImage(named: "monkey.png")
//        spotImageArray[indexPath.row].getDataInBackgroundWithBlock { (data, error) -> Void in
//            
//            if let downloadedImage = UIImage(data: data!) {
//                
//                cell.spotImage.image = downloadedImage
//                
//            }
//            
//        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        searchBar.resignFirstResponder()
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
            mapView.showsUserLocation = true
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius * 2, regionRadius * 2)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(mapView: MKMapView, didUpdateUserLocation userLocation: MKUserLocation) {
        
        if let loc = userLocation.location {
            centerMapOnLocation(loc)
        }
    }
    
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKindOfClass(Annotation) {
            let annoView = MKAnnotationView(annotation: annotation, reuseIdentifier: "Default")
            annoView.image = UIImage(named: "pin5.png")
            annoView.canShowCallout = true
            annoView.calloutOffset = CGPoint(x: -15, y: 15)
//            annoView.rightCalloutAccessoryView = UIButton(type: .DetailDisclosure) as UIView
            var leftImage: PFImageView = PFImageView(frame: CGRectMake(0, 0, 44, 44))
            leftImage.file = myAnnoImage
            leftImage.loadInBackground()
            annoView.leftCalloutAccessoryView = leftImage
            return annoView
        } else if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        return nil
    }
    
    func createAnnotationForLocation(spotName: String, location: CLLocation, spotImage: PFFile) {
        let spot = Annotation(title: spotName, coordinate: location.coordinate, image: spotImage)
        mapView.addAnnotation(spot)
    }
    
//    func getPlacemarkFromAddress(address: String) {
//        CLGeocoder().geocodeAddressString(address) { (placemarks: [CLPlacemark]?, error: NSError?) -> Void in
//            if let marks = placemarks where marks.count > 0 {
//                if let loc = marks[0].location {
//                    self.createAnnotationForLocation(loc)
//                }
//            }
//        }
//    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        searchBar.resignFirstResponder()
        return true
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            view.endEditing(true)
//            collectionView.reloadData()
        } else {
            inSearchMode = true
            let lower = searchBar.text!.lowercaseString
            
//            filteredResults = spots.filter({$0.spotName.rangeOfString(lower) != nil})
//            collectionView.reloadData()
        }
    }
    
}



    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


