//
//  accountViewController.swift
//  Skillspots
//
//  Created by Matt Barge on 8/11/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel
import MapKit

class accountVC: UIViewController, MKMapViewDelegate {

    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var userImage: UIImageView!

    let screenName = "Account"
    let locationManager = CLLocationManager()
    var locValue:CLLocationCoordinate2D!
    let geoCoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        designDetails()
        Mixpanel.sharedInstance().track("Account screen view")
        Mixpanel.sharedInstance().track("Screen View", properties: ["Screen" : screenName])
        // Do any additional setup after loading the view.
    }
    
    func designDetails() {
//        collectionView.delegate = self
//        collectionView.dataSource = self
        
        if let userUsername = PFUser.currentUser()?.username {
            userName.text = userUsername as String
        }
        
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locValue = locationManager.location?.coordinate
        let center = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        geoCoder.reverseGeocodeLocation(center, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                self.locationLabel.text = city as String
            }
            if let state = placeMark.addressDictionary!["State"] as? NSString {
                self.locationLabel.text = (self.locationLabel.text ?? "") + ", " + (state as String)
            }
            })
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {

        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutButtonTapped(sender: AnyObject) {
        PFUser.logOut()
        print("User signed out")
        self.performSegueWithIdentifier("goLogin", sender: self)
    }
    
    }
    
//    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("AccountCell", forIndexPath: indexPath) as? AccountCollectionViewCell {
//            
//            let spots = Spots(spotName: "Test", spotDescription: "Testing")
//            cell.configureCell(spots)
//            return cell
//        } else {
//            return UICollectionViewCell()
//        }
//    }
//    
//    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
//        
//    }
//    
//    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 10
//    }
//    
//    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1
//    }
//    
//    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
//        
//        return CGSizeMake(105, 105)
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
