//
//  homeTableViewController.swift
//  Skillspots
//
//  Created by Matt Barge on 8/11/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel
import MapKit

class homeVC: UITableViewController, MKMapViewDelegate {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var createSpotButton: UIBarButtonItem!
    var spotNameArray: [String] = []
    var spotDescriptionArray: [String] = []
    var spotImageArray: [PFFile] = []
    var spotCityArray: [String] = []
    var spotAuthorArray: [String] = []
    var spotDistanceArray: [Double] = []
    let screenName = "Home"
    var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,-200, 100, 100)) as UIActivityIndicatorView
    let locationManager = CLLocationManager()
    var currentLocation: PFGeoPoint!
    var locValue:CLLocationCoordinate2D!
    let test = "test"
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        locationAuthStatus()
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.actInd.startAnimating()
        designDetails()
        Mixpanel.sharedInstance().track("Screen View", properties: ["Screen" : screenName])
        Mixpanel.sharedInstance().identify("User")
        Mixpanel.sharedInstance().people.set("Plan", to: "Maximus")
        queryData()
    }

    func designDetails() {
        self.clearsSelectionOnViewWillAppear = false
        actInd.center = CGPointMake(self.view.center.x, self.view.center.y - 100)
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        actInd.color = UIColor.darkGrayColor()
        view.addSubview(actInd)
        if let font = UIFont(name: "Arial", size: 28) {
        createSpotButton.setTitleTextAttributes([NSFontAttributeName: font], forState: UIControlState.Normal)}
        navBar.barTintColor = UIColor.whiteColor()
        navBar.titleTextAttributes = [ NSFontAttributeName: UIFont(name: "Arial", size: 28)!]
        navBar.frame = CGRectMake(0, 0, 320, 40)
        self.view.addSubview(navBar)
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locValue = locationManager.location?.coordinate
        let center = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        currentLocation = PFGeoPoint(location: center)
    }
    
    func queryData() {
        let query = PFQuery(className: "Spots")
        query.selectKeys(["spotName", "spotDescription", "spotImage", "spotCity", "spotLocation", "spotAuthor"])
        
        // Retrieve the most recent ones
        query.orderByDescending("createdAt")
        
        // Only retrieve the last hundred
        query.limit = 20
        
        query.findObjectsInBackgroundWithBlock {
            (spot: [PFObject]?, error: NSError?) -> Void in
            if error != nil {
                print(error)
            } else {
                for spot in spot! {
                    // This does not require a network access.
                self.spotNameArray.append(spot["spotName"] as! String)
                self.spotDescriptionArray.append(spot["spotDescription"] as! String)
                self.spotImageArray.append(spot["spotImage"] as! PFFile)
                self.spotCityArray.append(spot["spotCity"] as! String)
                self.spotAuthorArray.append(spot["spotAuthor"] as! String)
                let spotGeoPoint = spot["spotLocation"] as! PFGeoPoint
                let distance = self.currentLocation.distanceInMilesTo(spotGeoPoint)
                self.spotDistanceArray.append(distance)
                self.tableView.reloadData()

                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return spotNameArray.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("homeCell", forIndexPath: indexPath) as! homeCell

        cell.selectionStyle = UITableViewCellSelectionStyle.None
        cell.spotTitle.text = spotNameArray[indexPath.row]
        cell.spotAuthor.text = spotAuthorArray[indexPath.row]
        cell.spotDescription.text = spotDescriptionArray[indexPath.row]
        cell.locationLabel.text = spotCityArray[indexPath.row]
        cell.distanceLabel.text = String(format:"%.1f", spotDistanceArray[indexPath.row]) + " mi"

        spotImageArray[indexPath.row].getDataInBackgroundWithBlock { (data, error) -> Void in
            
            if let downloadedImage = UIImage(data: data!) {
                
                let compressedImage = UIImage(data: downloadedImage.lowQualityJPEGNSData!)
                cell.spotImage.image = compressedImage
            }
            
        }
        self.actInd.stopAnimating()
        return cell
    }
    
    @IBAction func createSpotTapped(sender: AnyObject) {
        tabBarController?.selectedIndex = 1
    }
    
    func locationAuthStatus() {
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse {
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
