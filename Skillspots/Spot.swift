//
//  Spot.swift
//  Skillspots
//
//  Created by Matthew Barge on 11/14/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import Foundation
import Parse

class Spot {
    private var _spotLongitude: Float!
    private var _spotLatitude: Float!
    private var _spotName: String!
    private var _spotDescription: String!
    private var _spotImage: PFFile!
    
    var spotLongitude: Float {
        return _spotLongitude
    }
    
    var spotLatitude: Float {
        return _spotLatitude
    }
    
    var spotName: String {
        return _spotName
    }
    
    var spotDescription: String {
        return _spotDescription
    }
    
    var spotImage: PFFile {
        return _spotImage
    }
    
    init(spotLongitude: Float, spotLatitude: Float, spotName: String, spotDescription: String, spotImage: PFFile) {
        self._spotLongitude = spotLongitude
        self._spotLatitude = spotLatitude
        self._spotName = spotName
        self._spotDescription = spotDescription
        self._spotImage = spotImage
    }
}