//
//  signupVC.swift
//  Skillspots
//
//  Created by Matthew Barge on 1/25/17.
//  Copyright © 2017 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel

class signupVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet var allTextFields: [UITextField]!
    
    var errorMessage = "Please try again later"
    let screenName = "Signup"
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func designDetails() {
        self.view.addSubview(emailField)
        self.view.addSubview(usernameField)
        self.view.addSubview(passwordField)
        self.emailField.delegate = self
        self.usernameField.delegate = self
        self.passwordField.delegate = self
        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loginViewController.DismissKeyboard))
        //        view.addGestureRecognizer(tap)
        iconImage.layer.cornerRadius = 10
    }
    
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func validateFields() {
        if !emailField.text!.isEmpty && !passwordField.text!.isEmpty {
            signupButton.enabled = true
        }
    }
    
    @IBAction func textFieldEditChanged(sender: AnyObject) {
        self.validateFields()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        emailField.resignFirstResponder()
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        return true;
    }
    
    func signUp() {
        var user = PFUser()
        user.password = passwordField.text
        user.email = emailField.text
        user.username = usernameField.text
        // other fields can be set just like with PFObject
        
        user.signUpInBackgroundWithBlock {
            (succeeded: Bool, error: NSError?) -> Void in
            if let error = error {
                let errorString = error.userInfo["error"] as? NSString
                print("Registration error")
                // Show the errorString somewhere and let the user try again.
            } else {
                print(user.email! + " has registered")
                self.performSegueWithIdentifier("goHome", sender: self)
                // Hooray! Let them use the app now.
            }
        }
    }
    
    @IBAction func signupButtonTapped(sender: AnyObject) {
        signUp()
    }
    
    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
