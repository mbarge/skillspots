//
//  locationCell.swift
//  Skillspots
//
//  Created by Matthew Barge on 12/10/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit

class locationCell: UITableViewCell {

    @IBOutlet weak var spotImage: UIImageView!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotDescription: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
