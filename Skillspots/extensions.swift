//
//  imageSize.swift
//  Skillspots
//
//  Created by Matthew Barge on 12/15/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import Foundation
import UIKit
import Parse
import MapKit

extension PFGeoPoint {
    
    func location() -> CLLocation {
        return CLLocation(latitude: self.latitude, longitude: self.longitude)
    }
}

extension UIImage {
    var uncompressedPNGData: NSData?      { return UIImagePNGRepresentation(self)        }
    var highestQualityJPEGNSData: NSData? { return UIImageJPEGRepresentation(self, 1.0)  }
    var highQualityJPEGNSData: NSData?    { return UIImageJPEGRepresentation(self, 0.75) }
    var mediumQualityJPEGNSData: NSData?  { return UIImageJPEGRepresentation(self, 0.5)  }
    var lowQualityJPEGNSData: NSData?     { return UIImageJPEGRepresentation(self, 0.1) }
    var lowestQualityJPEGNSData:NSData?   { return UIImageJPEGRepresentation(self, 0.03)  }
    var thumbnailQualityJPEGNSData:NSData?   { return UIImageJPEGRepresentation(self, 0.1)  }
}

struct Analytics {
    static let button = "Button click"
    static let conversion = "Conversion"
    static let error = "Error"
    static let screen = "Screen view"
}