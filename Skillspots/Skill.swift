//
//  Skill.swift
//  Skillspots
//
//  Created by Matthew Barge on 11/14/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import Foundation

class Skill {
    private var _skillName: String!
    private var _skillDescription: String!
    
    var skillName: String {
        return _skillName
    }
    
    var skillDescription: String {
        return _skillDescription
    }
    
    init(skillName: String, skillDescription: String, spotLatitude: Float, spotLongitude: Float, spotID: Int) {
        self._skillName = skillName
        self._skillDescription = skillDescription
    }
}