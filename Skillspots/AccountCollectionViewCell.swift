//
//  AccountCollectionViewCell.swift
//  Skillspots
//
//  Created by Matthew Barge on 11/15/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit

class AccountCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellTextView: UITextView!
    
    var spot: Spot!
    
    func configureCell(spots: Spot) {
        self.spot = spots
        
        cellTextView.text = self.spot.spotName
        cellImage.image = UIImage(named: "")
    }
}
