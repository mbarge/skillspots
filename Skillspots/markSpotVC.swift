//
//  markSpotViewController.swift
//  Skillspots
//
//  Created by Matt Barge on 8/11/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel
import AddressBookUI
import AddressBook
import Contacts

class markSpotVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var createSpotLabel: UILabel!
    @IBOutlet weak var spotNameField: UITextField!
    @IBOutlet weak var spotDescriptionField: UITextField!
    @IBOutlet weak var uploadImageLabel: UILabel!
    @IBOutlet weak var chooseImageButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    
    var spotImage: PFFile!
    var imageSelected = false
    var fieldsFulfilled = false
    var imagePicker: UIImagePickerController!
    var actInd : UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(0,-200, 100, 100)) as UIActivityIndicatorView
    let screenName = "Mark spot"
    var eventName: String!
    var eventType: String!
    let locationManager = CLLocationManager()
    var spotLocation: PFGeoPoint!
    var locValue:CLLocationCoordinate2D!
    let geoCoder = CLGeocoder()
    var spotCity: String!
    var spotState: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        designDetails()
        Mixpanel.sharedInstance().track("Screen View", properties: ["Screen" : screenName])

        // Do any additional setup after loading the view.
    }
    
    func designDetails() {
        spotDescriptionField.borderStyle = UITextBorderStyle.RoundedRect
        imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        self.view.addSubview(spotNameField)
        self.view.addSubview(spotDescriptionField)
        self.spotNameField.delegate = self
        self.spotDescriptionField.delegate = self
        actInd.center = CGPointMake(self.view.center.x, self.view.center.y - 100)
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.WhiteLarge
        actInd.color = UIColor.darkGrayColor()
        view.addSubview(actInd)
        locationInformation()
        
        //        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loginViewController.DismissKeyboard))
//        view.addGestureRecognizer(tap)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "createdSpot" {
            if let destinationVC = segue.destinationViewController as? homeVC {
                destinationVC.tableView.reloadData()
                tabBarController?.selectedIndex = 0
            }
        }
    }
    
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        Mixpanel.sharedInstance().track("Dismiss Keyboard", properties: ["Screen" : screenName])
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        spotNameField.resignFirstResponder()
        spotDescriptionField.resignFirstResponder()
        return true;
    }

    func validateFields() {
        if spotNameField.text!.isEmpty {
            displayAlert("Name your spot", message: "Please give your spot a name!")
        } else if spotDescriptionField.text!.isEmpty {
            displayAlert("Describe your spot", message: "Please describe your spot!")
        } else if imageSelected == false {
            displayAlert("Select an image", message: "Please select an image from your camera roll or take a picture")
        } else {
            fieldsFulfilled = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func resetSpot() {
        spotNameField.text = ""
        spotDescriptionField.text = ""
    }

    @IBAction func resetButtonTapped(sender: AnyObject) {
        resetSpot()
        eventName = "Clear Spot Tapped"
        Mixpanel.sharedInstance().track(eventName, properties: ["Screen" : screenName, "Event Type" : Analytics.button])
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locValue = manager.location!.coordinate
        print("locations = \(locValue.latitude) \(locValue.longitude)")
    }
    
    func locationInformation() {
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        locValue = locationManager.location?.coordinate
        let center = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
        spotLocation = PFGeoPoint(location: center)
        geoCoder.reverseGeocodeLocation(center, completionHandler: { (placemarks, error) -> Void in
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                self.spotCity = city as String
            }
            if let state = placeMark.addressDictionary!["State"] as? NSString {
                self.spotState = state as String
            }
        })
    }
    
    func postSpot() {
        var spot = PFObject(className: "Spots")
        spot["spotName"] = spotNameField.text
        spot["spotDescription"] = spotDescriptionField.text
        spot["spotImage"] = spotImage
        spot["spotLocation"] = spotLocation
        spot["spotCity"] = spotCity as String + ", " + spotState as String
        if let currentUser = PFUser.currentUser()?.username {
            spot["spotAuthor"] = currentUser
        } else {
            spot["spotAuthor"] = "Anonymous"
        }
        spot.saveInBackgroundWithBlock { (success, error) -> Void in
            if success == true {
                self.actInd.stopAnimating()
                print("Spot created with ID: \(spot.objectId)")
                self.eventName = "Spot Created"
                Mixpanel.sharedInstance().track(self.eventName, properties: ["Screen" : self.screenName, "Event Type" : Analytics.conversion])
                self.displayAlert("Success!", message: "Your Skillspot has been created.")
                print("Created spot segue performed.")
            } else {
                print("Internet connection offline")
                self.displayAlert("Failure", message: "There was a problem creating your Skillspot. Please try again.")
            }
        }
    }
    
    @IBAction func postButtonTapped(sender: AnyObject) {
        validateFields()
        if fieldsFulfilled == true {
        self.actInd.startAnimating()
        postSpot()
        eventName = "Post Spot Tapped"
        Mixpanel.sharedInstance().track(eventName, properties: ["Screen" : screenName, "Event Type" : Analytics.button])
        } else {
        }
    }
    
    @IBAction func chooseImageTapped(sender: AnyObject) {
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func cameraTapped(sender: AnyObject) {
        eventName = "Camera tapped"
        Mixpanel.sharedInstance().track(eventName, properties: ["Screen" : screenName, "Event Type" : Analytics.button])
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        let okButton = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("createdSpot", sender: self)
        }
        alertController.addAction(okButton)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            var imageData: NSData
            if imagePicker.sourceType == .PhotoLibrary {
                imageData = pickedImage.lowQualityJPEGNSData!
            } else {
                imageData = pickedImage.lowestQualityJPEGNSData!
            }
            spotImage = PFFile(data: imageData)
            imageSelected = true
        }
        chooseImageButton.setTitle("Change Image", forState: UIControlState.Normal)
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
