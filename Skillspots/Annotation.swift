//
//  MapAnnotation.swift
//  Skillspots
//
//  Created by Matthew Barge on 12/12/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import Foundation
import MapKit
import Parse

class Annotation: NSObject, MKAnnotation {
    var coordinate = CLLocationCoordinate2D()
    let title: String?
    var image: PFFile!
    
    init(title: String, coordinate: CLLocationCoordinate2D, image: PFFile) {
        self.title = title
        self.coordinate = coordinate
        self.image = image
    }
}