//
//  homeTableViewCell.swift
//  Skillspots
//
//  Created by Matt Barge on 8/11/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit

class homeCell: UITableViewCell {

    @IBOutlet weak var spotImage: UIImageView!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotAuthor: UILabel!
    @IBOutlet weak var spotDescription: UITextView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
