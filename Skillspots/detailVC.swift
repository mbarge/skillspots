//
//  detailVC.swift
//  Skillspots
//
//  Created by Matthew Barge on 1/26/17.
//  Copyright © 2017 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel

class detailVC: UIViewController {
    
    @IBOutlet weak var spotImage: UIImageView!
    @IBOutlet weak var spotTitle: UILabel!
    @IBOutlet weak var spotAuthor: UILabel!
    @IBOutlet weak var spotDescription: UITextView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    let screenName = "Home"

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
