//
//  loginViewController.swift
//  Skillspots
//
//  Created by Matt Barge on 8/11/16.
//  Copyright © 2016 Matt Barge. All rights reserved.
//

import UIKit
import Parse
import Bolts
import Mixpanel

class loginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet var allTextFields: [UITextField]!
    
    var errorMessage = "Please try again later"
    let screenName = "Login"
//    var currentUser: PFUser?
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if PFUser.currentUser()?.email != nil {
            print(PFUser.currentUser()!.email! + " is already logged in")
            self.performSegueWithIdentifier("goHome", sender: self)
        } else {
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Mixpanel.sharedInstance().track("Screen View", properties: ["Screen" : screenName])
        
        self.designDetails()
        // Do any additional setup after loading the view.
    }
    
    func designDetails() {
        self.view.addSubview(usernameField)
        self.view.addSubview(passwordField)
        self.usernameField.delegate = self
        self.passwordField.delegate = self
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(loginViewController.DismissKeyboard))
//        view.addGestureRecognizer(tap)
        iconImage.layer.cornerRadius = 10
    }
    
    func DismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func validateFields() {
        if !usernameField.text!.isEmpty && !passwordField.text!.isEmpty {
            loginButton.enabled = true
            signupButton.enabled = true
        }
    }
    
    @IBAction func textFieldEditChanged(sender: AnyObject) {
        self.validateFields()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool // called when 'return' key pressed. return NO to ignore.
    {
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        return true;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func logIn() {
        var user = PFUser()
        user.password = passwordField.text
        user.username = usernameField.text
        
        PFUser.logInWithUsernameInBackground(user.username!, password: user.password!, block:
            { (user, error) -> Void in
                
                self.activityIndicator.stopAnimating()
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                
                if user != nil {
                    //Logged in
                    self.performSegueWithIdentifier("goHome", sender: self)
                } else {
                    if let errorString = error!.userInfo["error"] as? String {
                        self.errorMessage = errorString
                    }
                    self.displayAlert("Failed login", message: self.errorMessage)
                }
        })
    }
    
    @IBAction func logInButtonTapped(sender: AnyObject) {
        logIn()
    }
    
    func displayAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel,handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
